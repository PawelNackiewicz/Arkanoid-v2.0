﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Arkanoid_v2._0
{
    public partial class welcomeForm : Form
    {
        //lvl 0 easy 1 med 2 hard
        int lvl=1;
        public welcomeForm()
        {
            InitializeComponent();
        }
      

        private void new_game_btn_Click(object sender, EventArgs e)
        {
            if (lvl == 1)
            {
                gameForm ng = new gameForm();
                ng.Show();
            }
            else if(lvl==2)
            {
                gameForm ng = new gameForm(15);
                ng.Show();
            }
            else if (lvl==0)
            {
                gameForm ng = new gameForm(40);
                ng.Show();
            }
            
        }

        private void checkedListBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            if(button2.Text=="średni")
            {
                button2.Text = "trudny";
                button2.ForeColor = Color.Red;
                lvl = 2;
            }
            else if (button2.Text == "trudny")
            {
                button2.Text = "łatwy";
                button2.ForeColor = Color.Green;
                lvl = 0;
            }
            else if (button2.Text == "łatwy")
                {
                    button2.Text = "średni";
                    button2.ForeColor = Color.Blue;
                    lvl = 1;
                }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            infoForm info = new infoForm();
            info.Show();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }
    }
}
