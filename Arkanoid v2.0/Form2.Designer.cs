﻿namespace Arkanoid_v2._0
{
    partial class gameForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(gameForm));
            this.brick16 = new System.Windows.Forms.PictureBox();
            this.brick15 = new System.Windows.Forms.PictureBox();
            this.brick14 = new System.Windows.Forms.PictureBox();
            this.brick13 = new System.Windows.Forms.PictureBox();
            this.brick12 = new System.Windows.Forms.PictureBox();
            this.brick11 = new System.Windows.Forms.PictureBox();
            this.brick10 = new System.Windows.Forms.PictureBox();
            this.brick7 = new System.Windows.Forms.PictureBox();
            this.brick9 = new System.Windows.Forms.PictureBox();
            this.brick8 = new System.Windows.Forms.PictureBox();
            this.brick6 = new System.Windows.Forms.PictureBox();
            this.brick5 = new System.Windows.Forms.PictureBox();
            this.brick4 = new System.Windows.Forms.PictureBox();
            this.brick3 = new System.Windows.Forms.PictureBox();
            this.brick2 = new System.Windows.Forms.PictureBox();
            this.brick1 = new System.Windows.Forms.PictureBox();
            this.ball_img = new System.Windows.Forms.PictureBox();
            this.paddle_img = new System.Windows.Forms.PictureBox();
            this.tlo_img = new System.Windows.Forms.PictureBox();
            this.ball_timer = new System.Windows.Forms.Timer(this.components);
            this.r_timer = new System.Windows.Forms.Timer(this.components);
            this.l_timer = new System.Windows.Forms.Timer(this.components);
            this.gameOver_btn = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.brick16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.brick15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.brick14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.brick13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.brick12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.brick11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.brick10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.brick7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.brick9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.brick8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.brick6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.brick5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.brick4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.brick3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.brick2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.brick1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ball_img)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.paddle_img)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tlo_img)).BeginInit();
            this.SuspendLayout();
            // 
            // brick16
            // 
            this.brick16.Image = ((System.Drawing.Image)(resources.GetObject("brick16.Image")));
            this.brick16.InitialImage = ((System.Drawing.Image)(resources.GetObject("brick16.InitialImage")));
            this.brick16.Location = new System.Drawing.Point(454, 228);
            this.brick16.Name = "brick16";
            this.brick16.Size = new System.Drawing.Size(60, 60);
            this.brick16.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.brick16.TabIndex = 34;
            this.brick16.TabStop = false;
            // 
            // brick15
            // 
            this.brick15.Image = ((System.Drawing.Image)(resources.GetObject("brick15.Image")));
            this.brick15.InitialImage = ((System.Drawing.Image)(resources.GetObject("brick15.InitialImage")));
            this.brick15.Location = new System.Drawing.Point(522, 162);
            this.brick15.Name = "brick15";
            this.brick15.Size = new System.Drawing.Size(60, 60);
            this.brick15.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.brick15.TabIndex = 33;
            this.brick15.TabStop = false;
            // 
            // brick14
            // 
            this.brick14.Image = ((System.Drawing.Image)(resources.GetObject("brick14.Image")));
            this.brick14.InitialImage = ((System.Drawing.Image)(resources.GetObject("brick14.InitialImage")));
            this.brick14.Location = new System.Drawing.Point(454, 162);
            this.brick14.Name = "brick14";
            this.brick14.Size = new System.Drawing.Size(60, 60);
            this.brick14.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.brick14.TabIndex = 32;
            this.brick14.TabStop = false;
            // 
            // brick13
            // 
            this.brick13.Image = ((System.Drawing.Image)(resources.GetObject("brick13.Image")));
            this.brick13.InitialImage = ((System.Drawing.Image)(resources.GetObject("brick13.InitialImage")));
            this.brick13.Location = new System.Drawing.Point(388, 162);
            this.brick13.Name = "brick13";
            this.brick13.Size = new System.Drawing.Size(60, 60);
            this.brick13.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.brick13.TabIndex = 31;
            this.brick13.TabStop = false;
            // 
            // brick12
            // 
            this.brick12.Image = ((System.Drawing.Image)(resources.GetObject("brick12.Image")));
            this.brick12.InitialImage = ((System.Drawing.Image)(resources.GetObject("brick12.InitialImage")));
            this.brick12.Location = new System.Drawing.Point(588, 96);
            this.brick12.Name = "brick12";
            this.brick12.Size = new System.Drawing.Size(60, 60);
            this.brick12.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.brick12.TabIndex = 30;
            this.brick12.TabStop = false;
            // 
            // brick11
            // 
            this.brick11.Image = ((System.Drawing.Image)(resources.GetObject("brick11.Image")));
            this.brick11.InitialImage = ((System.Drawing.Image)(resources.GetObject("brick11.InitialImage")));
            this.brick11.Location = new System.Drawing.Point(522, 96);
            this.brick11.Name = "brick11";
            this.brick11.Size = new System.Drawing.Size(60, 60);
            this.brick11.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.brick11.TabIndex = 29;
            this.brick11.TabStop = false;
            // 
            // brick10
            // 
            this.brick10.Image = ((System.Drawing.Image)(resources.GetObject("brick10.Image")));
            this.brick10.InitialImage = ((System.Drawing.Image)(resources.GetObject("brick10.InitialImage")));
            this.brick10.Location = new System.Drawing.Point(454, 96);
            this.brick10.Name = "brick10";
            this.brick10.Size = new System.Drawing.Size(60, 60);
            this.brick10.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.brick10.TabIndex = 28;
            this.brick10.TabStop = false;
            // 
            // brick7
            // 
            this.brick7.Image = ((System.Drawing.Image)(resources.GetObject("brick7.Image")));
            this.brick7.InitialImage = ((System.Drawing.Image)(resources.GetObject("brick7.InitialImage")));
            this.brick7.Location = new System.Drawing.Point(654, 30);
            this.brick7.Name = "brick7";
            this.brick7.Size = new System.Drawing.Size(60, 60);
            this.brick7.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.brick7.TabIndex = 27;
            this.brick7.TabStop = false;
            // 
            // brick9
            // 
            this.brick9.Image = ((System.Drawing.Image)(resources.GetObject("brick9.Image")));
            this.brick9.InitialImage = ((System.Drawing.Image)(resources.GetObject("brick9.InitialImage")));
            this.brick9.Location = new System.Drawing.Point(388, 96);
            this.brick9.Name = "brick9";
            this.brick9.Size = new System.Drawing.Size(60, 60);
            this.brick9.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.brick9.TabIndex = 26;
            this.brick9.TabStop = false;
            // 
            // brick8
            // 
            this.brick8.Image = ((System.Drawing.Image)(resources.GetObject("brick8.Image")));
            this.brick8.InitialImage = ((System.Drawing.Image)(resources.GetObject("brick8.InitialImage")));
            this.brick8.Location = new System.Drawing.Point(322, 96);
            this.brick8.Name = "brick8";
            this.brick8.Size = new System.Drawing.Size(60, 60);
            this.brick8.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.brick8.TabIndex = 25;
            this.brick8.TabStop = false;
            // 
            // brick6
            // 
            this.brick6.Image = ((System.Drawing.Image)(resources.GetObject("brick6.Image")));
            this.brick6.InitialImage = ((System.Drawing.Image)(resources.GetObject("brick6.InitialImage")));
            this.brick6.Location = new System.Drawing.Point(588, 30);
            this.brick6.Name = "brick6";
            this.brick6.Size = new System.Drawing.Size(60, 60);
            this.brick6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.brick6.TabIndex = 24;
            this.brick6.TabStop = false;
            // 
            // brick5
            // 
            this.brick5.Image = ((System.Drawing.Image)(resources.GetObject("brick5.Image")));
            this.brick5.Location = new System.Drawing.Point(522, 30);
            this.brick5.Name = "brick5";
            this.brick5.Size = new System.Drawing.Size(60, 60);
            this.brick5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.brick5.TabIndex = 23;
            this.brick5.TabStop = false;
            // 
            // brick4
            // 
            this.brick4.Image = ((System.Drawing.Image)(resources.GetObject("brick4.Image")));
            this.brick4.InitialImage = ((System.Drawing.Image)(resources.GetObject("brick4.InitialImage")));
            this.brick4.Location = new System.Drawing.Point(454, 30);
            this.brick4.Name = "brick4";
            this.brick4.Size = new System.Drawing.Size(60, 60);
            this.brick4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.brick4.TabIndex = 22;
            this.brick4.TabStop = false;
            // 
            // brick3
            // 
            this.brick3.Image = ((System.Drawing.Image)(resources.GetObject("brick3.Image")));
            this.brick3.InitialImage = ((System.Drawing.Image)(resources.GetObject("brick3.InitialImage")));
            this.brick3.Location = new System.Drawing.Point(388, 30);
            this.brick3.Name = "brick3";
            this.brick3.Size = new System.Drawing.Size(60, 60);
            this.brick3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.brick3.TabIndex = 21;
            this.brick3.TabStop = false;
            // 
            // brick2
            // 
            this.brick2.Image = ((System.Drawing.Image)(resources.GetObject("brick2.Image")));
            this.brick2.InitialImage = ((System.Drawing.Image)(resources.GetObject("brick2.InitialImage")));
            this.brick2.Location = new System.Drawing.Point(322, 30);
            this.brick2.Name = "brick2";
            this.brick2.Size = new System.Drawing.Size(60, 60);
            this.brick2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.brick2.TabIndex = 20;
            this.brick2.TabStop = false;
            // 
            // brick1
            // 
            this.brick1.Image = ((System.Drawing.Image)(resources.GetObject("brick1.Image")));
            this.brick1.InitialImage = ((System.Drawing.Image)(resources.GetObject("brick1.InitialImage")));
            this.brick1.Location = new System.Drawing.Point(256, 30);
            this.brick1.Name = "brick1";
            this.brick1.Size = new System.Drawing.Size(60, 60);
            this.brick1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.brick1.TabIndex = 19;
            this.brick1.TabStop = false;
            // 
            // ball_img
            // 
            this.ball_img.BackColor = System.Drawing.Color.Transparent;
            this.ball_img.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.ball_img.Image = ((System.Drawing.Image)(resources.GetObject("ball_img.Image")));
            this.ball_img.Location = new System.Drawing.Point(454, 400);
            this.ball_img.Margin = new System.Windows.Forms.Padding(0);
            this.ball_img.Name = "ball_img";
            this.ball_img.Size = new System.Drawing.Size(42, 42);
            this.ball_img.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.ball_img.TabIndex = 35;
            this.ball_img.TabStop = false;
            // 
            // paddle_img
            // 
            this.paddle_img.Image = ((System.Drawing.Image)(resources.GetObject("paddle_img.Image")));
            this.paddle_img.Location = new System.Drawing.Point(414, 647);
            this.paddle_img.Name = "paddle_img";
            this.paddle_img.Size = new System.Drawing.Size(133, 26);
            this.paddle_img.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.paddle_img.TabIndex = 36;
            this.paddle_img.TabStop = false;
            // 
            // tlo_img
            // 
            this.tlo_img.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.tlo_img.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tlo_img.Location = new System.Drawing.Point(0, 0);
            this.tlo_img.Name = "tlo_img";
            this.tlo_img.Size = new System.Drawing.Size(958, 699);
            this.tlo_img.TabIndex = 37;
            this.tlo_img.TabStop = false;
            // 
            // ball_timer
            // 
            this.ball_timer.Enabled = true;
            this.ball_timer.Interval = 30;
            this.ball_timer.Tick += new System.EventHandler(this.Ball_timer_Tick);
            // 
            // r_timer
            // 
            this.r_timer.Interval = 20;
            this.r_timer.Tick += new System.EventHandler(this.r_timer_Tick);
            // 
            // l_timer
            // 
            this.l_timer.Interval = 20;
            this.l_timer.Tick += new System.EventHandler(this.l_timer_Tick);
            // 
            // gameOver_btn
            // 
            this.gameOver_btn.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.gameOver_btn.Location = new System.Drawing.Point(256, 265);
            this.gameOver_btn.Name = "gameOver_btn";
            this.gameOver_btn.Size = new System.Drawing.Size(444, 123);
            this.gameOver_btn.TabIndex = 38;
            this.gameOver_btn.Text = "koniec gry";
            this.gameOver_btn.UseVisualStyleBackColor = true;
            this.gameOver_btn.Visible = false;
            this.gameOver_btn.Click += new System.EventHandler(this.gameOver_btn_Click);
            // 
            // gameForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(958, 699);
            this.Controls.Add(this.gameOver_btn);
            this.Controls.Add(this.paddle_img);
            this.Controls.Add(this.ball_img);
            this.Controls.Add(this.brick16);
            this.Controls.Add(this.brick15);
            this.Controls.Add(this.brick14);
            this.Controls.Add(this.brick13);
            this.Controls.Add(this.brick12);
            this.Controls.Add(this.brick11);
            this.Controls.Add(this.brick10);
            this.Controls.Add(this.brick7);
            this.Controls.Add(this.brick9);
            this.Controls.Add(this.brick8);
            this.Controls.Add(this.brick6);
            this.Controls.Add(this.brick5);
            this.Controls.Add(this.brick4);
            this.Controls.Add(this.brick3);
            this.Controls.Add(this.brick2);
            this.Controls.Add(this.brick1);
            this.Controls.Add(this.tlo_img);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow;
            this.Name = "gameForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Arkanoid";
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.gameForm_KeyDown);
            this.KeyUp += new System.Windows.Forms.KeyEventHandler(this.gameForm_KeyUp);
            ((System.ComponentModel.ISupportInitialize)(this.brick16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.brick15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.brick14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.brick13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.brick12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.brick11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.brick10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.brick7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.brick9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.brick8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.brick6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.brick5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.brick4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.brick3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.brick2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.brick1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ball_img)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.paddle_img)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tlo_img)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox brick16;
        private System.Windows.Forms.PictureBox brick15;
        private System.Windows.Forms.PictureBox brick14;
        private System.Windows.Forms.PictureBox brick13;
        private System.Windows.Forms.PictureBox brick12;
        private System.Windows.Forms.PictureBox brick11;
        private System.Windows.Forms.PictureBox brick10;
        private System.Windows.Forms.PictureBox brick7;
        private System.Windows.Forms.PictureBox brick9;
        private System.Windows.Forms.PictureBox brick8;
        private System.Windows.Forms.PictureBox brick6;
        private System.Windows.Forms.PictureBox brick5;
        private System.Windows.Forms.PictureBox brick4;
        private System.Windows.Forms.PictureBox brick3;
        private System.Windows.Forms.PictureBox brick2;
        private System.Windows.Forms.PictureBox brick1;
        private System.Windows.Forms.PictureBox ball_img;
        private System.Windows.Forms.PictureBox paddle_img;
        private System.Windows.Forms.PictureBox tlo_img;
        private System.Windows.Forms.Timer ball_timer;
        private System.Windows.Forms.Timer r_timer;
        private System.Windows.Forms.Timer l_timer;
        private System.Windows.Forms.Button gameOver_btn;
    }
}