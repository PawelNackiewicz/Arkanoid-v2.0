﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Arkanoid_v2._0
{
    public partial class gameForm : Form
    {
        public gameForm(int level=30)
        {
            InitializeComponent();
            ball_timer.Interval = level;
        }
        bool kolizja(PictureBox imgBall, PictureBox imgBrick)
        {
            if (imgBall.Left >= imgBrick.Left - imgBall.Width &&
               imgBall.Left <= imgBrick.Left + imgBrick.Width &&
               imgBall.Top >= imgBrick.Top - imgBall.Height &&
               imgBall.Top <= imgBrick.Top + imgBrick.Height)
            {
                return true;
            }
            else return false;
        }
        int x = -8, y = -8, to_win = 16;
      

        private void gameForm_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Right) r_timer.Enabled = true;
            if (e.KeyCode == Keys.Left) l_timer.Enabled = true;
        }

        private void gameForm_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Right) r_timer.Enabled = false;
            if (e.KeyCode == Keys.Left) l_timer.Enabled = false;
        }

        private void Ball_timer_Tick(object sender, EventArgs e)
        {
            ball_img.Left += x;
            ball_img.Top += y;
            //odbijanie od lewej sciany
            if ((ball_img.Left - 5) <= tlo_img.Left) x = -x;
            //odbijanie od gornej sciany
            if ((ball_img.Top - 5) <= tlo_img.Top) y = -y;
            //odbijanie od prawej sciany
            if ((ball_img.Left + ball_img.Width + 5) >= tlo_img.Width) x = -x;
            //skucha
            if (ball_img.Top >= paddle_img.Top + paddle_img.Height + 15)
            {
                ball_timer.Enabled = false;
                ball_img.Visible = false;
                gameOver_btn.Text = "Koniec gry! Porażka !";
                gameOver_btn.Visible = true;
            
            }
            else if (ball_img.Left > paddle_img.Left - (ball_img.Width / 2) && ball_img.Left < paddle_img.Left + paddle_img.Width && ball_img.Top + ball_img.Height > paddle_img.Top)
            {
                if (y > 0) y = -y;
            }
            if (to_win <= 0)
            {
                ball_timer.Enabled = false;
                ball_img.Visible = false;
                gameOver_btn.Text = "Koniec gry! Wygrana !";
                gameOver_btn.Visible = true;
            }

            //blok1
            if (kolizja(ball_img, brick1) && brick1.Visible == true)
            {
                x = -x;
                y = -y;
                brick1.Visible = false;
                to_win--;
            }
            //blok2
            if (kolizja(ball_img, brick2) && brick2.Visible == true)
            {
                x = -x;
                y = -y;
                brick2.Visible = false;
                to_win--;
            }
            //blok3
            if (kolizja(ball_img, brick3) && brick3.Visible == true)
            {
                x = -x;
                y = -y;
                brick3.Visible = false;
                to_win--;
            }
            //blok4
            if (kolizja(ball_img, brick4) && brick4.Visible == true)
            {
                x = -x;
                y = -y;
                brick4.Visible = false;
                to_win--;
            }
            //blok5
            if (kolizja(ball_img, brick5) && brick5.Visible == true)
            {
                x = -x;
                y = -y;
                brick5.Visible = false;
                to_win--;
            }
            //blok6
            if (kolizja(ball_img, brick6) && brick6.Visible == true)
            {
                x = -x;
                y = -y;
                brick6.Visible = false;
                to_win--;
            }
            //blok7
            if (kolizja(ball_img, brick7) && brick7.Visible == true)
            {
                x = -x;
                y = -y;
                brick7.Visible = false;
                to_win--;
            }
            //blok8
            if (kolizja(ball_img, brick8) && brick8.Visible == true)
            {
                x = -x;
                y = -y;
                brick8.Visible = false;
                to_win--;
            }
            //blok9
            if (kolizja(ball_img, brick9) && brick9.Visible == true)
            {
                x = -x;
                y = -y;
                brick9.Visible = false;
                to_win--;
            }
            //blok10
            if (kolizja(ball_img, brick10) && brick10.Visible == true)
            {
                x = -x;
                y = -y;
                brick10.Visible = false;
                to_win--;
            }
            //blok11
            if (kolizja(ball_img, brick11) && brick11.Visible == true)
            {
                x = -x;
                y = -y;
                brick11.Visible = false;
                to_win--;
            }
            //blok12
            if (kolizja(ball_img, brick12) && brick12.Visible == true)
            {
                x = -x;
                y = -y;
                brick12.Visible = false;
                to_win--;
            }
            //blok13
            if (kolizja(ball_img, brick13) && brick13.Visible == true)
            {
                x = -x;
                y = -y;
                brick13.Visible = false;
                to_win--;
            }
            //blok14
            if (kolizja(ball_img, brick14) && brick14.Visible == true)
            {
                x = -x;
                y = -y;
                brick14.Visible = false;
                to_win--;
            }
            //blok15
            if (kolizja(ball_img, brick15) && brick15.Visible == true)
            {
                x = -x;
                y = -y;
                brick15.Visible = false;
                to_win--;
            }
            //blok16
            if (kolizja(ball_img, brick16) && brick16.Visible == true)
            {
                x = -x;
                y = -y;
                brick16.Visible = false;
                to_win--;
            }
        
    }

        private void gameOver_btn_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void r_timer_Tick(object sender, EventArgs e)
        {
            if (paddle_img.Left + paddle_img.Width < tlo_img.Width - 10) paddle_img.Left += 10;
        }

        private void l_timer_Tick(object sender, EventArgs e)
        {
            if (paddle_img.Left > 10) paddle_img.Left -= 10;
        }
    }
}
